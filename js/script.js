$(document).ready(function () {
    $(".getting-started").countdown('2018/05/01 12:00:00', function (event) {
        var $this = $(this).html(event.strftime('' + '<div class="date-number">%D <div class="date-title">Days</div></div>:' + '<div class="date-number">%H <div class="date-title">Hours</div></div>:' + '<div class="date-number">%M <div class="date-title">Minutes</div></div>:' + '<div class="date-number">%S <div class="date-title">Seconds</div></div>'));
    });

    var currentX = '';
    var currentY = '';
    var movementConstant = .015;
    $(document).mousemove(function(e) {
        if(currentX == '') currentX = e.pageX;
        if(currentY == '') currentY = e.pageY;
        var xdiff = e.pageX - currentX;
        var ydiff = e.pageY - currentY;
        currentX = e.pageX;
        currentY = e.pageY;
        $('.parallax').each(function(i, el) {
            var movement = (i + 1) * (xdiff * movementConstant);
            var movementY = (i + 1) * (ydiff * movementConstant);
            var newX = $(el).position().left + movement;
            var newY = $(el).position().top + movementY;
            $(el).css('left', newX + 'px');
            $(el).css('top', newY + 'px');
        });
    });
});
